<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
    'name',
    'date',
    'city',
    'lat',
    'lng',
    'address',
    'content',
    'image',
    'tickets'
    ]; // this means the title field can be mass assigned

    protected $dates = ['date']; // stops this date being automatically converted to a Carbon instance

    public function scopePublished($query) {
    	$query->where('date', '>=', Carbon::now()->format('Y-m-d'));
    }
    public function scopeUnpublished($query) {
        $query->where('date', '<', Carbon::now()->format('Y-m-d'));
    }
}