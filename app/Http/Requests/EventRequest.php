<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true; //currently anyone can make this request
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'date' => 'required|date',
            'city' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'address' => 'required',
            'content' => 'required|min:3',
            'image' => 'required',
            'tickets' => 'required'
        ];
    }
}
