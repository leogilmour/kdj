<?php

namespace App\Http\Controllers;

use App\Video;

use App\Http\Requests;

use App\Http\Requests\VideoRequest;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except'=>['index', 'show', 'next']]);
    }

    public function index()
    {
    	$videos = Video::orderBy('order', 'asc')->get();

    	return view('videos.index', compact('videos'));
    }

    public function show($id)
	{
		$video = Video::findOrFail($id);

		return view('videos.show', compact('video'));
	}
    public function create()
    {
        $items = Video::orderBy('order', 'asc')->get();
        return view('videos.create', compact('items'));
    }

    public function store(VideoRequest $request)
    {
        // $input = Request::all();

        // Event::create(Request::all());

        Video::create($request->all());

        return redirect('gallery');
    }

    public function edit($id)
    {
        $video = Video::findOrFail($id);

        $items = Video::orderBy('order', 'asc')->get();

        return view('videos.edit', compact('video', 'items'));
    }

    public function update($id, VideoRequest $request)
    {
        $video = Video::findOrFail($id);

        $video->update($request->all());

        return redirect('gallery');
    }

    public function next($id)
    {
        $previous = Video::find($id);

        $order = $previous->order + 1;

        $video = Video::where('order', $order)->first();

        if ($video)
            return redirect()->route('gallery.show', compact('video'));
        else {
            $video = Video::where('order', 1)->first();
            return redirect()->route('gallery.show', compact('video'));
        }
    }

    public function destroy($id)
    {
        $video = Video::findOrFail($id);
        
        $video->delete();
        
        return redirect('gallery');
    }
}
