<?php

namespace App\Http\Controllers;

use App\Event;

use App\Http\Requests;

use App\Http\Requests\EventRequest;

use Illuminate\Support\Facades\Auth;


class UpcomingController extends Controller
{
	public function __construct()
	{
		// $this->middleware('auth', ['only'=>'create']);
		$this->middleware('auth', ['except'=>['index', 'show']]);
	}

	public function index()
	{
		$events = Event::orderBy('date', 'asc')->published()->get();
		if (Auth::check())
			$prevs = Event::orderBy('date', 'asc')->unpublished()->get();

		return view('upcoming.index', compact('events', 'prevs'));
	}

	public function show($id)
	{
		$event = Event::findOrFail($id);
//		dd($event->date);

		return view('upcoming.show', compact('event'));
	}

	public function create()
	{
		return view('upcoming.create');
	}

	public function store(EventRequest $request)
	{
		// $input = Request::all();

		// Event::create(Request::all());

		Event::create($request->all());

		return redirect('upcoming');
	}

	public function edit($id)
	{
		$event = Event::findOrFail($id);

		return view('upcoming.edit', compact('event'));
	}

	public function update($id, EventRequest $request)
	{
		$event = Event::findOrFail($id);

		$event->update($request->all());

		return redirect('upcoming');
	}

	public function destroy($id)
	{
		$event = Event::findOrFail($id);

		$event->delete();
		
		return redirect('upcoming');
	}
};

















