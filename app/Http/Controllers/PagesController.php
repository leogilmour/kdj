<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class PagesController extends Controller
{
	public function about()
	{
		$first = 'Katya';
		$last = 'DJ';
		return view('pages.about', compact('first', 'last'));
	}

	public function contact()
	{
		$email = 'katya@kdj.com';
		$phone = '';

		$data = [];

		$data['email'] = $email;
		$data['phone'] = $phone;

		return view('pages.contact', $data);
	}
}