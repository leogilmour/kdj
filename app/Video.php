<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'gallery'; // assumed table was videos

    protected $fillable = [
    'name',
    'url',
    'order',
    'start'
    ]; // this means the title field can be mass assigned
}