@extends('layouts.layout')
@section('styles')
	<link href="/css/admin.css" rel="stylesheet">
@stop
@section('content')

	<h1>
		Edit Event: {{ $event->name }}
	</h1>
	<br>

	{!! Form::model($event, ['method' => 'PATCH', 'action' => ['UpcomingController@update', $event->id]]) !!}
		@include ('upcoming.form', [
			'datePrefillText' 	=> $event->date,
			'latDefaultText'	=> null,
			'lngDefaultText'	=> null,
			'contentDefaultText' 	=> null,
			'imageDefaultText'	=> null,
			'ticketsDefaultText'	=> null,
			'submitButtonText' 	=> 'Update upcoming event'
		])
	{!! Form::close() !!}

	@include ('errors.list')

	-<br><br>
    	{!! Form::model($event, ['method' => 'DELETE', 'action' => ['UpcomingController@destroy', $event->id]]) !!}
	    	{!! Form::submit('Delete this event', ['class' => 'btn btn-primary delete']) !!}
	{!! Form::close() !!}

	<br>
@stop
