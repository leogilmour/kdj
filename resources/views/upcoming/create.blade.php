@extends('layouts.layout')
@section('styles')
	<link href="/css/admin.css" rel="stylesheet">
@stop
@section('content')

	<h1>
		Create a new event
	</h1>
	<br>

	{!! Form::open(['url' => 'upcoming']) !!}
		@include ('upcoming.form', [
			'submitButtonText'	=> 'Add upcoming event',
			'datePrefillText'	=> date('Y-m-d'),
			'latDefaultText'	=> 51.5073,
			'lngDefaultText'	=> -0.12755,
			'contentDefaultText'	=> 'First paragraph.</p><p>Second paragraph.',
			'imageDefaultText'	=> 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcREEyqXrarxgfba8dmFLCvNAvYD_ZHQWZHD_R_d57qHuU5j8t7sbQ',
			'ticketsDefaultText'	=> 'https://www.eventbrite.co.uk/'
		])
	{!! Form::close() !!}

	@include ('errors.list')

@stop