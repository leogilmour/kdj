@extends('layouts.layout')

@section('styles')
	<link href="/css/upcoming.css" rel="stylesheet">
@stop

@section('content')
	<h1>Events</h1>
	@foreach ($events as $event)
		<br>
		<a href="{{ url('/upcoming', $event->id) }}">
			<article
				@if ($event->created_at >= \Carbon\Carbon::now()->subWeek()->format('Y-m-d'))
					class="new"
				@endif"
			>			
				<h2 class="name">{{ $event->name }}</h2>
				<br>
				<h3><span class="date">{{ date('d M y', strtotime($event->date)) }}</span><span class="bullet"> · </span><span class="city">{{ $event->city }}</span></h3>
			</article>
		</a>
		<br>
		<br>
	@endforeach

	<br>
	<br>

	@if (Auth::check())
	<h2>_Previous Events_</h2>
	@foreach ($prevs as $prev)
		<a href="{{ url('/upcoming', $prev->id) }}">
			<article class="old">
				<h2 class="name">{{ $prev->name }}</h2>
				<br>
				<h3><span class="date">{{ date('d M y', strtotime($prev->date)) }}</span><span class="bullet"> · </span><span class="city">{{ $prev->city }}</span></h3>
			</article>
		</a>
		<br>
		<br>
	@endforeach

	<br>
	<br>

	@endif

@stop