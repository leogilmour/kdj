@extends('layouts.layout')

@section('styles')
	<link href="/css/upcoming.css" rel="stylesheet">
@stop

@section('content')
	@if (Auth::check())
	<br><a href="/upcoming/{{ $event->id }}/edit">Edit this event</a>
	@endif
	<div id="invitation">
		<h1>{{ $event->name }}</h1>
		<h2>{{ $event->city }}</h2>
		<h3>{{ date('d F y', strtotime($event->date)) }}</h3>
			<!-- <h3>{{ $event->date->diffForHumans() }} -->
		<br>
		<a class="ticket-button" target="_blank" href="{{ $event->tickets }}">Buy tickets here</a>
		<article>
			<p>{!! $event->content !!}</p>
			<p>{{ $event->address }}</p>
			<!-- myLat + ',' + myLng -->
			<div id="map"></div>
		</article>
	</div>
	<br>
	<br>
@stop

@section('js')
<script>
	var map;
	function initMap() {
		var myLat = {{ $event->lat }};
		var myLng = {{ $event->lng }};
		var myLatLng = {lat: myLat, lng: myLng};
		console.log(myLatLng);
		map = new google.maps.Map(document.getElementById('map'), {
			center: myLatLng,
			zoom: 12
		});
		var image = '/images/marker.png';
		var contentString = '<div id="content" style="background-image: url({{ $event->image }});">'+
					'<h1 id="firstHeading" class="firstHeading">{{ $event->name }}</h1>'+
					'<a class="map-button" target="_blank" href="https://www.google.com/maps/search/{{ $event->address }}">'+
					'Open in maps</a>'+
					'</div>';
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: '{{ $event->name }}',
			animation: google.maps.Animation.DROP,
			icon: image
		});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLMXq2t3paCjiC8feKfudLW3wRYXRpPwA&callback=initMap"
async defer></script>
@stop