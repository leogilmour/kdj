<div>
	{!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', null, ['class' => 'form-control', 'test' => 'attribute']) !!} <!-- null parameter here is the default value for the cell -->
</div>
<div>
	{!! Form::label('date', 'Date:') !!}
	{!! Form::date('date', $datePrefillText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('city', 'City:') !!}
	{!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('address', 'Address:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('lat', 'Lattitude:') !!}
	{!! Form::text('lat', $latDefaultText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('lng', 'Longitude:') !!}
	{!! Form::text('lng', $lngDefaultText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('content', 'Content:') !!}
	{!! Form::textarea('content', $contentDefaultText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('image', 'Image:') !!}
	{!! Form::text('image', $imageDefaultText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('tickets', 'Tickets:') !!}
	{!! Form::text('tickets', $ticketsDefaultText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>