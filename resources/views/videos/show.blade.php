@extends('layouts.layout')
@section('styles')
	<link href="/css/video.css" rel="stylesheet">
@stop
@section('content')
@if (Auth::check())
	<br><a href="/gallery/{{ $video->id }}/edit">Edit this video</a>
@endif

<article>
	<h1>{{ $video->name }}</h1><a href="/gallery/next/{{ $video->id }}">Play next</a>
		<div id="player"></div>
	<!-- <div class="screen">
		<iframe width="560" height="315" src="{{ $video->url }}?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
	</div> -->
</article>

@stop

@section('js')
<script>
	  // 2. This code loads the IFrame Player API code asynchronously.
	  var tag = document.createElement('script');

	  tag.src = "https://www.youtube.com/iframe_api";
	  var firstScriptTag = document.getElementsByTagName('script')[0];
	  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

	  // 3. This function creates an <iframe> (and YouTube player)
	  //    after the API code downloads.
	  var player;
	  function onYouTubeIframeAPIReady() {
		player = new YT.Player('player', {
		  height: '390',
		  width: '640',
		  videoId: '{{ $video->url }}',
		  playerVars: {
			rel: 0,
			showinfo: 0,
			autoplay: 1,
			color: 'white',
			disablekb: 0,
			start: {{ $video->start }}
		  },
		  events: {
			'onStateChange': onPlayerStateChange
		  }
		});
	  }
	  function onPlayerStateChange(event) {
		if (event.data == YT.PlayerState.ENDED) {
			window.location.href = "/gallery/next/{{ $video->id }}";
		}
	  }
	</script>
@stop