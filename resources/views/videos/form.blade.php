<div>
	{!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!} <!-- null parameter here is the default value for the cell -->
</div>
<div>
	{!! Form::label('url', 'URL:') !!}
	{!! Form::text('url', $urlPrefillText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('order', 'Order:') !!}
	{!! Form::text('order', null, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::label('start', 'Start (in seconds):') !!}
	{!! Form::text('start', $startPrefillText, ['class' => 'form-control']) !!}
</div>
<div>
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
</div>