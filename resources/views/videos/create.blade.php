@extends('layouts.layout')
@section('styles')
	<link href="/css/admin.css" rel="stylesheet">
@stop
@section('content')

	<h1>
		Create a new video
	</h1>
	<br>

	{!! Form::open(['url' => 'gallery']) !!}
		@include ('videos.form', [
			'submitButtonText'	=> 'Add video',
			'urlPrefillText'	=> 'utXJ2JsUHvg',
			'startPrefillText' 	=> 0,
			])
	{!! Form::close() !!}

	@include ('errors.list')

	<br>
	<h4>Current playlist order:</h4>
	<ol>
	    	@foreach ($items as $item)
		    	<li value="{{ $item->order }}">{{ $item->name }}</li>
	    	@endforeach
	</ol>
	<br>
	<br>

@stop