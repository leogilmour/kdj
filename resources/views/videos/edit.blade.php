@extends('layouts.layout')
@section('styles')
	<link href="/css/admin.css" rel="stylesheet">
@stop
@section('content')

	<h1>
		Edit Video: {{ $video->name }}
	</h1>
	<br>

	{!! Form::model($video, ['method' => 'PATCH', 'action' => ['GalleryController@update', $video->id]]) !!}
		@include ('videos.form', [
			'urlPrefillText' 	=> $video->url,
			'startPrefillText' 	=> $video->start,
			'submitButtonText' 	=> 'Update video'
		])
	{!! Form::close() !!}

	@include ('errors.list')

	<br>
	<h4>Current playlist order:</h4>
	<ol>
		@foreach ($items as $item)
		<li value="{{ $item->order }}">{{ $item->name }}</li>
		@endforeach
	</ol>
    	<br><br>
    	{!! Form::model($video, ['method' => 'DELETE', 'action' => ['GalleryController@destroy', $video->id]]) !!}
	    	{!! Form::submit('Delete this video', ['class' => 'btn btn-primary delete']) !!}
	{!! Form::close() !!}

@stop
