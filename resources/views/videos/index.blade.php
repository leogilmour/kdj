@extends('layouts.layout')
@section('styles')
	<link href="/css/video.css" rel="stylesheet">
@stop
@section('content')

	<h1>Music</h1>
	<div class="collection">
		
		@foreach ($videos as $video)
			
			<a class="record" href="/gallery/{{ $video->id }}">
				<h2
				@if ($video->created_at >= \Carbon\Carbon::now()->subWeek()->format('Y-m-d'))
					class="new"
				@endif
				>
					{{ $video->name }}</h2>
			</a>
		@endforeach
	</div>

@stop