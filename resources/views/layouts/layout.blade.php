<!doctype html>
<html lang='en'>

<head>
	@include('includes.head')

	@yield('styles')
</head>

<body>
	<header class="row">
		@include('includes.header')
	</header>
	<div class="container">
		

		<div id="main" class="row">
			@yield('content')
		</div>

		
	</div>
	<footer class="row">
		<script src="/js/app.js"></script>
		
		@yield('js')

		@yield('footer')
	</footer>
</body>

</html>