@extends('layouts.layout')
@section('styles')
<link href="/css/contact.css" rel="stylesheet">
@stop
@section('content')

<h1>Get in touch</h1>
<div class="contact">
	<a id="email" href="mailto:{{ $email }}?Subject=Contact%20Katya" target="_top">{{ $email }}</a>
</div>

<h2>Follow me on</h2>
<div class="social-media">
	<a id="yt" href="https://www.youtube.com/channel/UCNht43ME1oEFT0z6lYzijIA"></a>
	<a id ="fb" href="https://www.facebook.com/KatyaDJ"></a>
</div>
@stop