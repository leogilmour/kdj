<div class="links">
	<a href="/about">about</a>
	<a href="/contact">contact</a>
	<a href="/">KATYA DJ</a>
	<a
		@if ($count = App\Video::where('created_at','>=', \Carbon\Carbon::now()->subWeek()->format('Y-m-d'))->count() > 0)
			class="new"
		@endif
	href="/gallery">records</a>
	<a
		@if ($count = App\Event::where('created_at','>=', \Carbon\Carbon::now()->subWeek()->format('Y-m-d'))->count() > 0)
			class="new"
		@endif
	href="/upcoming">upcoming</a>
</div>
<hr>
@if (Auth::check())
	<ul class="admin-menu" role="menu">
		<li>
			<a href="/gallery/create">New video</a>
		</li>
		<li>
			<a href="/upcoming/create">New event</a>
		</li>
		<li>
			<a href="{{ url('/logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
			Logout
			</a>
			<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
				{{ csrf_field() }}
			</form>
		</li>
	</ul>
<hr>
@endif
