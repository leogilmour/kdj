<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('pages.welcome');
});

Route::get('about', 'PagesController@about');

Route::get('contact', 'PagesController@contact');

Route::resource('gallery', 'GalleryController');
Route::get('gallery/next/{id}', 'GalleryController@next');

// Route::get('upcoming', 'UpcomingController@index');
// Route::get('upcoming/create', 'UpcomingController@create');
// Route::get('upcoming/{id}', 'UpcomingController@show');
// Route::post('upcoming', 'UpcomingController@store');

Route::resource('upcoming', 'UpcomingController');

Auth::routes();

Route::get('/home', 'HomeController@index');

// Route::get('foo', ['middleware' => 'admin', function()
// {
// 	return 'this page may only be viewed by admin';

// }]);